<?php

namespace Drupal\Tests\fasttoggle\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the fasttoggle module.
 *
 * @group fasttoggle
 */
class FasttoggleSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'fasttoggle',
    'node',
    'comment',
  ];

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $adminUser;

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user;

  /**
   * Settings form url.
   */
  protected Url $settingsRoute;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->settingsRoute = Url::fromRoute('fasttoggle.settings');

    $this->user = $this->DrupalCreateUser();

    $this->adminUser = $this->DrupalCreateUser([
      'administer fasttoggle',
    ]);
  }

  /**
   * Tests that the settings page can be reached and saved.
   */
  public function testSettingsPage() {
    $this->drupalLogin($this->user);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(200);

    $edit = [
      'label_style' => '0',
    ];
    $expected_values = [
      'label_style' => '0',
    ];

    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    foreach ($expected_values as $field => $expected_value) {
      $actual_value = $this->config('fasttoggle.settings')->get($field);
      $this->assertEquals($expected_value, $actual_value);
    }
  }

}

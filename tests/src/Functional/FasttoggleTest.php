<?php

namespace Drupal\Tests\fasttoggle\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\comment\Entity\Comment;
use Drupal\comment\Tests\CommentTestTrait;

/**
 * Tests for the fasttoggle module.
 *
 * @group fasttoggle
 */
class FasttoggleTest extends BrowserTestBase {

  use CommentTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'fasttoggle',
    'node',
    'comment',
  ];

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * The comment storage.
   *
   * @var \Drupal\comment\CommentStorageInterface
   */
  protected $commentStorage;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->user = $this->DrupalCreateUser([
      'use fasttoggle',
      'access comments',
      'post comments',
    ]);

    $this->nodeStorage = $this->container->get('entity_type.manager')->getStorage('node');
    $this->commentStorage = $this->container->get('entity_type.manager')->getStorage('comment');
  }

  /**
   * Tests node fasttoggle.
   */
  public function testNodeFasttoggle() {
    $page = $this->createContentType(['type' => 'page']);
    $page->setThirdPartySetting('fasttoggle', 'promote', TRUE);
    $page->setThirdPartySetting('fasttoggle', 'sticky', TRUE);
    $page->setThirdPartySetting('fasttoggle', 'status', TRUE);
    $page->save();

    $node = $this->createNode([
      'title' => 'Test node',
      'type' => 'page',
    ]);

    $this->nodeStorage->resetCache([$node->id()]);
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->nodeStorage->load($node->id());
    $this->assertTrue($node->isPublished(), 'Node is published');
    $this->assertTrue($node->isPromoted(), 'Node is promoted');
    $this->assertFalse($node->isSticky(), 'Node is not sticky');

    $this->drupalLogin($this->user);
    $this->drupalGet('/node/' . $node->id());
    $this->clickLink('Demote');
    $this->drupalGet('/node/' . $node->id());
    $this->clickLink('Make sticky');
    $this->drupalGet('/node/' . $node->id());
    $this->clickLink('Unpublish');

    $this->nodeStorage->resetCache([$node->id()]);
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->nodeStorage->load($node->id());
    $this->assertFalse($node->isPublished(), 'Node is not published');
    $this->assertFalse($node->isPromoted(), 'Node is not promoted');
    $this->assertTrue($node->isSticky(), 'Node is sticky');
  }

  /**
   * Tests comment fasttoggle.
   */
  public function testCommentFasttoggle() {
    $this->createContentType(['type' => 'page']);
    $this->addDefaultCommentField('node', 'page');
    /** @var \Drupal\comment\CommentTypeInterface $comment_type */
    $comment_type = $this->container->get('entity_type.manager')->getStorage('comment_type')->load('comment');
    $comment_type->setThirdPartySetting('fasttoggle', 'status', TRUE);
    $comment_type->save();

    $node = $this->createNode([
      'title' => 'Test node',
      'type' => 'page',
    ]);

    $comment = Comment::create([
      'entity_type' => 'node',
      'name' => 'Tony',
      'hostname' => 'magic.example.com',
      'mail' => 'foo@example.com',
      'subject' => 'Comment on node',
      'entity_id' => $node->id(),
      'comment_type' => 'comment',
      'field_name' => 'comment',
      'pid' => 0,
      'uid' => $this->user->id(),
      'status' => 1,
    ]);
    $comment->save();

    $this->commentStorage->resetCache([$comment->id()]);
    /** @var \Drupal\comment\CommentInterface $comment */
    $comment = $this->commentStorage->load($comment->id());
    $this->assertTrue($comment->isPublished(), 'Comment is published');

    $this->drupalLogin($this->user);
    $this->drupalGet('/node/' . $node->id());
    $this->clickLink('Unpublish');

    $this->commentStorage->resetCache([$comment->id()]);
    /** @var \Drupal\comment\CommentInterface $comment */
    $comment = $this->commentStorage->load($comment->id());
    $this->assertFalse($comment->isPublished(), 'Comment is not published');
  }

}

<?php

namespace Drupal\fasttoggle\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure fasttoggle settings for this site.
 */
class FasttoggleSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fasttoggle_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fasttoggle.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('fasttoggle.settings');

    $form['label_style'] = [
      '#type' => 'radios',
      '#title' => $this->t('Label style'),
      '#description' => $this->t('Select what kind of labels you want for fasttoggle links.'),
      '#options' => [
        0 => $this->t('Status (reflects the current state, e.g. "published", "active")'),
        1 => $this->t('Action (shows what happens upon a click, e.g. "unpublish", "block")'),
      ],
      '#default_value' => $config->get('label_style'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('fasttoggle.settings')
      ->set('label_style', $values['label_style'])
      ->save();

    $this->messenger()->addMessage($this->t('The configuration options have been saved.'));
  }

}

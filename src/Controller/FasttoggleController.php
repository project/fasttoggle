<?php

namespace Drupal\fasttoggle\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Route controller for Fasttoggle.
 */
class FasttoggleController extends ControllerBase {

  /**
   * Toggle a setting.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   */
  public function toggle(Request $request) {
    $parts = explode('/', $request->getPathInfo());
    $index = array_search('fasttoggle', $parts);
    $type = $parts[$index + 1];
    $id = $parts[$index + 2];
    $action = $parts[$index + 3];
    $config = $this->config('fasttoggle.settings');
    $label_style = $config->get('label_style');

    if (count($parts) !== 4 && count($parts) !== 5 && count($parts) !== 6) {
      $response = new Response();
      $response->setContent('Incorrect number of parameters');
      $response->setStatusCode(400);
      return $response;
    }

    $link_title = '';
    $selector = '';
    $data = '';

    switch ($type) {
      case 'node':
        $storage_handler = $this->entityTypeManager()->getStorage('node');
        /** @var \Drupal\node\NodeInterface $node */
        $node = $storage_handler->load($id);

        if (!$node) {
          $response = new Response();
          $response->setContent('Invalid entity ID.');
          $response->setStatusCode(400);
          return $response;
        }

        switch ($action) {
          case 'status':
            $status = $node->isPublished();
            switch ($label_style) {
              case 0:
                !$node->isPublished() ? $link_title = $this->t('Published') : $link_title = $this->t('Not published');
                break;

              case 1:
                !$node->isPublished() ? $link_title = $this->t('Unpublish') : $link_title = $this->t('Publish');
                break;
            }
            $status ? $node->setUnpublished() : $node->setPublished();
            $selector = '.fasttoggle-node-status';
            $data = '<a href="/fasttoggle/node/' . $id . '/status" class="use-ajax fasttoggle-node-status">' . $link_title . '</a>';
            break;

          case 'promote':
            $promote = $node->isPromoted();
            switch ($label_style) {
              case 0:
                !$node->isPromoted() ? $link_title = $this->t('Promoted') : $link_title = $this->t('Not promoted');
                break;

              case 1:
                !$node->isPromoted() ? $link_title = $this->t('Demote') : $link_title = $this->t('Promote');
                break;
            }
            $node->setPromoted(!$promote);
            $selector = '.fasttoggle-node-promote';
            $data = '<a href="/fasttoggle/node/' . $id . '/promote" class="use-ajax fasttoggle-node-promote">' . $link_title . '</a>';
            break;

          case 'sticky':
            $sticky = $node->isSticky();
            switch ($label_style) {
              case 0:
                !$node->isSticky() ? $link_title = $this->t('Sticky') : $link_title = $this->t('Not sticky');
                break;

              case 1:
                !$node->isSticky() ? $link_title = $this->t('Make not sticky') : $link_title = $this->t('Make sticky');
                break;
            }
            $node->setSticky(!$sticky);
            $selector = '.fasttoggle-node-sticky';
            $data = '<a href="/fasttoggle/node/' . $id . '/sticky" class="use-ajax fasttoggle-node-sticky">' . $link_title . '</a>';
            break;
        }

        $node->save();

        $response = new AjaxResponse();
        $response->addCommand(new ReplaceCommand($selector, $data, NULL));
        return $response;

      case 'comment':
        $storage_handler = $this->entityTypeManager()->getStorage('comment');
        /** @var \Drupal\comment\CommentInterface $comment */
        $comment = $storage_handler->load($id);

        if (!$comment) {
          $response = new Response();
          $response->setContent('Invalid entity ID.');
          $response->setStatusCode(400);
          return $response;
        }

        if ($action === 'status') {
          $status = $comment->isPublished();
          switch ($label_style) {
            case 0:
              !$comment->isPublished() ? $link_title = $this->t('Published') : $link_title = $this->t('Not published');
              break;

            case 1:
              !$comment->isPublished() ? $link_title = $this->t('Unpublish') : $link_title = $this->t('Publish');
              break;
          }
          $status ? $comment->setUnpublished() : $comment->setPublished();
          $class = 'fasttoggle-comment-' . $id . '-status';
          $selector = '.' . $class;
          $data = '<a href="/fasttoggle/comment/' . $id . '/status" class="use-ajax ' . $class . '">' . $link_title . '</a>';
        }

        $comment->save();

        $response = new AjaxResponse();
        $response->addCommand(new ReplaceCommand($selector, $data, NULL));
        return $response;

      default:
        $response = new Response();
        $response->setContent('Invalid entity type.');
        $response->setStatusCode(400);
        return $response;
    }
  }

}

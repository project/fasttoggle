# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

Fasttoggle is a Drupal module that simplifies and speeds the task of site
administration providing ajax toggling of both administrative settings and
content values.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/fasttoggle>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/fasttoggle>


## REQUIREMENTS

Drupal core nodes and Drupal core comments.


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configure > System > Fasttoggle
    3. Navigate to Administration > Structure > Content Types and turning on 
       Fasttoggle in the your content type
    4. Save configuration.


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
